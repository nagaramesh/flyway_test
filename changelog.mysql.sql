--liquibase formatted sql

--changeset ramesh:1
create table test3.sample1 (id INT(6), firstname VARCHAR(30));

--rollback drop table sample1;

--changeset ramesh:1.2
INSERT INTO test3.sample1 (id,firstname) VALUES (3,"test3");
INSERT INTO test3.sample1 (id,firstname) VALUES (4,"test4");


--rollback delete from sample1 where id=3;
--rollback delete from sample1 where id=4;


--changeset ramesh:2
INSERT INTO test3.sample1 (id,firstname) VALUES (1,"test1");
INSERT INTO test3.sample1 (id,firstname) VALUES (2,"test2");


--rollback delete from sample1 where id=1;
--rollback delete from sample1 where id=2;


--changeset ramesh:4
INSERT INTO test3.sample1 (id,firstname) VALUES (5,"test5");
INSERT INTO test3.sample1 (id,firstname) VALUES (6,"test6");

--changeset ramesh:3
INSERT INTO test3.sample1 (id,firstname) VALUES (7,"test7");
INSERT INTO test3.sample1 (id,firstname) VALUES (8,"test8");